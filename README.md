# Хранилища и Базы данных

### Запуск

1. docker compose up -d
2. Автоматически создадуться объекты в БД и заполнит тестовыми данными
3. Airflow доступен по адресу http://localhost:8080/ (логин/пароль - airflow:airflow)
4. Во вкладке Admin --> Variables проверить наличие следующих ключей:
   1. API_URL=http://api-service:8080/api/v1
   2. MONGO_URL=mongodb://mongo:mongo@mongo:27017/db-storage?authSource=admin
   3. PG_URL=postgresql+psycopg2://postgres:postgres@postgres:5432/db_labs

Если указанные ключи отсутствуют, необходимо либо добавить их руками, либо запустить контейнер db-storage-airflow-init-1

5. Во вкладке DAG можно посмотреть на работу. Все DAG-и кроме build_cdm запускаются каждые 10 минут. build_cdm оставлен
   в ручном режиме для удобства тестов.

### Подключение

## PostgreSQL

+ host: `localhost`
+ port: `5432`
+ username: `postgres`
+ password: `postgres`
+ db-name: `db_labs`
+ Используемые схемы данных: `cdm`, `dds`, `stg`, `src`

## MongoDB

+ host: `localhost`
+ port: `27017`
+ username: `mongo`
+ password: `mongo`
+ db-name: `db-storage`
+ Коллекции: `Clients`,`Orders`,`Restaurant`

## API

API реализовано с использованием springboot. Для добавления новых данных необходимо внести изменения в
файлы `delivery.json` и `deliveryman.json` в директории `/api/src/main/resource`

После внесения изменений можно перезагрузить сервис с помощью команды: `docker compose up -d --build --force-recreate`

![lab1-scheme](./Diag.jpg)