db = new Mongo().getDB('db-storage');

db.createCollection('Orders');
db.createCollection('Clients');
db.createCollection('Restaurant');

db.Clients.insertMany(
    [
        {
            '_id': ObjectId('6673e2abfc13ae710e234882'),
            'name': 'Guillermo Shirt',
            'phone': '4608015667',
            'birthday': '03.08.1997',
            'email': 'gshirt0@time.com',
            'login': 'gshirt0',
            'address': '3326 Armistice Road',
            'update_time': ISODate("2024-03-11T17:47:23.000Z")
        },
        {
            '_id': ObjectId('6673e2abfc13ae710e234883'),
            'name': 'Elisha Bilton',
            'phone': '5471823114',
            'birthday': '20.11.2001',
            'email': 'ebilton1@msu.edu',
            'login': 'ebilton1',
            'address': '178 Birchwood Hill',
            'update_time': ISODate("2024-03-15T17:47:23.000Z")
        },
        {
            '_id': ObjectId('6673e2abfc13ae710e234884'),
            'name': 'Hildagarde Scotchford',
            'phone': '9894606290',
            'birthday': '24.07.1993',
            'email': 'hscotchford2@google.it',
            'login': 'hscotchford2',
            'address': '12 Melrose Alley',
            'update_time': ISODate("2024-03-16T17:47:23.000Z")
        }
    ]
)

db.Restaurant.insertMany(
    [
        {
            '_id': ObjectId('6673e67bfc13ae74a9234abc'),
            'name': 'Galapagos NV',
            'phone': '6793756143',
            'email': 'cfrayn0@ezinearticles.com',
            'founding_day': '25.07.2017',
            'menu': [
                {
                    '_id': ObjectId('6673e7c5fc13ae7442234d5b'),
                    'name': 'Beef - Bones, Cut - Up',
                    'price': 87.17,
                    'dish_category': 'category #5'
                },
                {
                    '_id': ObjectId('6673e7c5fc13ae7442234d5c'),
                    'name': 'Pasta - Cannelloni, Sheets, Fresh',
                    'price': 196.14,
                    'dish_category': 'category #1'
                },
                {
                    '_id': ObjectId('6673e7c5fc13ae7442234d5d'),
                    'name': 'Placemat - Scallop, White',
                    'price': 144.06,
                    'dish_category': 'category #1'
                },
                {
                    '_id': ObjectId('6673e7c5fc13ae7442234d5e'),
                    'name': 'Wine - Jackson Triggs Okonagan',
                    'price': 85.46,
                    'dish_category': 'category #2'
                },
                {
                    '_id': ObjectId('6673e7c5fc13ae7442234d5f'),
                    'name': 'Island Oasis - Lemonade',
                    'price': 115.88,
                    'dish_category': 'category #2'
                }
            ],
            'update_time': ISODate("2024-01-11T17:47:23.000Z")
        },
        {
            '_id': ObjectId('6673e67bfc13ae74a9234abd'),
            'name': 'NovoCure Limited',
            'phone': '9853288488',
            'email': 'amacallester1@naver.com',
            'founding_day': '26.02.2010',
            'menu': [
                {
                    '_id': ObjectId('6673e7c5fc13ae7442234d5a'),
                    'name': 'Seedlings - Mix, Organic',
                    'price': 50.97,
                    'dish_category': 'category #1'
                },
                {
                    '_id': ObjectId('6673e7c5fc13ae7442234d60'),
                    'name': 'Squid Ink',
                    'price': 70.3,
                    'dish_category': 'category #3'
                },
                {
                    '_id': ObjectId('6673e7c5fc13ae7442234d61'),
                    'name': 'Nut - Pecan, Halves',
                    'price': 162.55,
                    'dish_category': 'category #1'
                },
                {
                    '_id': ObjectId('6673e7c5fc13ae7442234d62'),
                    'name': 'Sprite - 355 Ml',
                    'price': 166.52,
                    'dish_category': 'category #3'
                },
                {
                    '_id': ObjectId('6673e7c5fc13ae7442234d63'),
                    'name': 'Salt - Celery',
                    'price': 198.14,
                    'dish_category': 'category #1'
                },
                {
                    '_id': ObjectId('6673e7c5fc13ae7442234d64'),
                    'name': 'Wine - Magnotta, Merlot Sr Vqa',
                    'price': 199.44,
                    'dish_category': 'category #4'
                }
            ],
            'update_time': ISODate("2023-12-22T17:47:23.000Z")
        },
        {
            '_id': ObjectId('6673e67bfc13ae74a9234abe'),
            'name': 'KCAP Financial, Inc.',
            'phone': '3913830512',
            'email': 'kcroxley2@nydailynews.com',
            'founding_day': '23.12.2012',
            'menu': [
                {
                    '_id': ObjectId('6673e7c5fc13ae7442234d65'),
                    'name': 'Tray - Foam, Square 4 - S',
                    'price': 31.18,
                    'dish_category': 'category #2'
                },
                {
                    '_id': ObjectId('6673e7c5fc13ae7442234d66'),
                    'name': 'Cheese - St. Andre',
                    'price': 70.19,
                    'dish_category': 'category #2'
                },
                {
                    '_id': ObjectId('6673e7c5fc13ae7442234d67'),
                    'name': 'Water - San Pellegrino',
                    'price': 94.68,
                    'dish_category': 'category #4'
                }
            ],
            'update_time': ISODate("2024-03-11T17:47:23.000Z")
        },
        {
            '_id': ObjectId('6673e67bfc13ae74a9234abf'),
            'name': 'Cisco Systems, Inc.',
            'phone': '9455075118',
            'email': 'tjantot3@sitemeter.com',
            'founding_day': '29.07.2018',
            'menu': [
                {
                    '_id': ObjectId('6673e7c5fc13ae7442234d68'),
                    'name': 'Chicken - Breast, 5 - 7 Oz',
                    'price': 77.94,
                    'dish_category': 'category #1'
                },
                {
                    '_id': ObjectId('6673e7c5fc13ae7442234d69'),
                    'name': 'Stock - Chicken, White',
                    'price': 50.45,
                    'dish_category': 'category #3'
                },
                {
                    '_id': ObjectId('6673e7c5fc13ae7442234d6a'),
                    'name': 'Longos - Chicken Cordon Bleu',
                    'price': 41.58,
                    'dish_category': 'category #4'
                },
                {
                    '_id': ObjectId('6673e7c5fc13ae7442234d6b'),
                    'name': 'Pork - Back, Long Cut, Boneless',
                    'price': 96.27,
                    'dish_category': 'category #3'
                },
                {
                    '_id': ObjectId('6673e7c5fc13ae7442234d6c'),
                    'name': 'Lighter - Bbq',
                    'price': 24.07,
                    'dish_category': 'category #5'
                },
                {
                    '_id': ObjectId('6673e7c5fc13ae7442234d6d'),
                    'name': 'Guinea Fowl',
                    'price': 88.79,
                    'dish_category': 'category #4'
                },
                {
                    '_id': ObjectId('6673e7c5fc13ae7442234d6e'),
                    'name': 'Water - Aquafina Vitamin',
                    'price': 198.86,
                    'dish_category': 'category #3'
                }
            ],
            'update_time': ISODate("2024-03-01T17:47:23.000Z")
        },
        {
            '_id': ObjectId('6673e67bfc13ae74a9234ac0'),
            'name': 'Amerco',
            'phone': '9418497885',
            'email': 'jstonman4@exblog.jp',
            'founding_day': '25.01.2010',
            'menu': [
                {
                    '_id': ObjectId('6673e7c5fc13ae7442234d6f'),
                    'name': 'Pork Ham Prager',
                    'price': 27.25,
                    'dish_category': 'category #5'
                },
                {
                    '_id': ObjectId('6673e7c5fc13ae7442234d70'),
                    'name': 'Chicken - Leg / Back Attach',
                    'price': 51.55,
                    'dish_category': 'category #4'
                },
                {
                    '_id': ObjectId('6673e7c5fc13ae7442234d71'),
                    'name': 'Oyster - In Shell',
                    'price': 85.65,
                    'dish_category': 'category #4'
                },
                {
                    '_id': ObjectId('6673e7c5fc13ae7442234d72'),
                    'name': 'Bamboo Shoots - Sliced',
                    'price': 167.35,
                    'dish_category': 'category #3'
                },
                {
                    '_id': ObjectId('6673e7c5fc13ae7442234d73'),
                    'name': 'Wine - Piper Heidsieck Brut',
                    'price': 80.38,
                    'dish_category': 'category #3'
                },
                {
                    '_id': ObjectId('6673e7c5fc13ae7442234d74'),
                    'name': 'Pastry - Plain Baked Croissant',
                    'price': 166.84,
                    'dish_category': 'category #4'
                },
                {
                    '_id': ObjectId('6673e7c5fc13ae7442234d75'),
                    'name': 'Octopus - Baby, Cleaned',
                    'price': 75.19,
                    'dish_category': 'category #1'
                },
                {
                    '_id': ObjectId('6673e7c5fc13ae7442234d76'),
                    'name': 'Pork - Side Ribs',
                    'price': 118.55,
                    'dish_category': 'category #4'
                },
                {
                    '_id': ObjectId('6673e7c5fc13ae7442234d77'),
                    'name': 'Cheese - Fontina',
                    'price': 52.13,
                    'dish_category': 'category #3'
                }
            ],
            'update_time': ISODate("2024-02-11T17:47:23.000Z")
        }
    ]
)

db.Orders.insertMany(
    [
        {
            '_id': ObjectId('6673ef41fc13ae710e2348f0'),
            'restaurant': {'_id': ObjectId('6673e67bfc13ae74a9234abc')},
            'order_date': '24.04.2024',
            'client': {'_id': ObjectId('6673e2abfc13ae710e234882')},
            'ordered_dish': [
                {
                    'id': ObjectId('6673e7c5fc13ae7442234d5b'),
                    'name': 'Beef - Bones, Cut - Up',
                    'price': 87.17,
                    'quantity': 3
                },
                {
                    'id': ObjectId('6673e7c5fc13ae7442234d5e'),
                    'name': 'Wine - Prosecco Valdobienne',
                    'price': 85.46,
                    'quantity': 2
                },
                {
                    'id': ObjectId('6673e7c5fc13ae7442234d5f'),
                    'name': 'Bandage - Fexible 1x3',
                    'price': 115.88,
                    'quantity': 1
                }
            ],
            'payed_by_bonuses': 100,
            'cost': 548.31,
            'payment': 548.31,
            'bonus_for_visit': 5,
            'statuses': [
                {
                    'status': 'CLOSE',
                    'time': ISODate("2024-04-24T14:17:52.000Z")
                },
                {
                    'status': 'DELIVERING',
                    'time': ISODate("2024-04-24T13:17:52.000Z")
                },
                {
                    'status': 'COOKING',
                    'time': ISODate("2024-04-24T12:17:52.000Z")
                },
                {
                    'status': 'OPEN',
                    'time': ISODate("2024-04-24T11:17:52.000Z")
                }
            ],
            'final_status': 'CLOSE',
            'update_time': ISODate("2024-04-24T15:17:52.000Z")
        },
        {
            '_id': ObjectId('6673ef41fc13ae710e2348f1'),
            'restaurant': {'_id': ObjectId('6673e67bfc13ae74a9234abc')},
            'order_date': '15.05.2024',
            'client': {'_id': ObjectId('6673e2abfc13ae710e234883')},
            'ordered_dish': [
                {
                    'id': ObjectId('6673e7c5fc13ae7442234d5e'),
                    'name': 'Wine - Jackson Triggs Okonagan',
                    'price': 85.46,
                    'quantity': 1
                }
            ],
            'payed_by_bonuses': 0,
            'cost': 85.46,
            'payment': 85.46,
            'bonus_for_visit': 5,
            'statuses': [
                {
                    'status': 'OPEN',
                    'time': ISODate("2024-05-15T19:41:20.000Z")
                }
            ],
            'final_status': 'OPEN',
            'update_time': ISODate("2024-05-15T19:41:20.000Z")
        },
        {
            '_id': ObjectId('6673ef41fc13ae710e2348f2'),
            'restaurant': {'_id': ObjectId('6673e67bfc13ae74a9234abc')},
            'order_date': '15.03.2024',
            'client': {'_id': ObjectId('6673e2abfc13ae710e234884')},
            'ordered_dish': [
                {
                    'id': ObjectId('6673e7c5fc13ae7442234d5d'),
                    'name': 'Placemat - Scallop, White',
                    'price': 144.06,
                    'quantity': 1
                },
                {
                    'id': ObjectId('6673e7c5fc13ae7442234d5e'),
                    'name': 'Wine - Jackson Triggs Okonagan',
                    'price': 85.46,
                    'quantity': 2
                }
            ],
            'payed_by_bonuses': 0,
            'cost': 314.98,
            'payment': 314.98,
            'bonus_for_visit': 5,
            'statuses': [
                {
                    'status': 'COOKING',
                    'time': ISODate("2024-03-15T17:47:23.000Z")
                },
                {
                    'status': 'OPEN',
                    'time': ISODate("2024-03-15T16:47:23.000Z")
                }
            ],
            'final_status': 'COOKING',
            'update_time': ISODate("2024-03-15T17:47:23.000Z")
        },
        {
            '_id': ObjectId('6673ef41fc13ae710e2348f3'),
            'restaurant': {'_id': ObjectId('6673e67bfc13ae74a9234abd')},
            'order_date': '11.03.2024',
            'client': {'_id': ObjectId('6673e2abfc13ae710e234882')},
            'ordered_dish': [
                {
                    'id': ObjectId('6673e7c5fc13ae7442234d5a'),
                    'name': 'Seedlings - Mix, Organic',
                    'price': 50.97,
                    'quantity': 4
                },
                {
                    'id': ObjectId('6673e7c5fc13ae7442234d60'),
                    'name': 'Squid Ink',
                    'price': 70.3,
                    'quantity': 2
                },
                {
                    'id': ObjectId('6673e7c5fc13ae7442234d61'),
                    'name': 'Nut - Pecan, Halves',
                    'price': 162.55,
                    'quantity': 2
                },
                {
                    'id': ObjectId('6673e7c5fc13ae7442234d62'),
                    'name': 'Sprite - 355 Ml',
                    'price': 166.52,
                    'quantity': 2
                },
                {
                    'id': ObjectId('6673e7c5fc13ae7442234d63'),
                    'name': 'Salt - Celery',
                    'price': 198.14,
                    'quantity': 3
                }
            ],
            'payed_by_bonuses': 350,
            'cost': 1597.04,
            'payment': 1597.04,
            'bonus_for_visit': 400,
            'statuses': [
                {
                    'status': 'OPEN',
                    'time': ISODate("2024-03-11T17:47:23.000Z")
                }
            ],
            'final_status': 'OPEN',
            'update_time': ISODate("2024-03-11T17:47:23.000Z")
        },
        {
            '_id': ObjectId('6673ef41fc13ae710e2348f4'),
            'restaurant': {'_id': ObjectId('6673e67bfc13ae74a9234abd')},
            'order_date': '23.01.2024',
            'client': {'_id': ObjectId('6673e2abfc13ae710e234883')},
            'ordered_dish': [
                {
                    'id': ObjectId('6673e7c5fc13ae7442234d5a'),
                    'name': 'Seedlings - Mix, Organic',
                    'price': 50.97,
                    'quantity': 3
                },
                {
                    'id': ObjectId('6673e7c5fc13ae7442234d60'),
                    'name': 'Squid Ink',
                    'price': 70.3,
                    'quantity': 2
                },
                {
                    'id': ObjectId('6673e7c5fc13ae7442234d61'),
                    'name': 'Nut - Pecan, Halves',
                    'price': 162.55,
                    'quantity': 2
                },
                {
                    'id': ObjectId('6673e7c5fc13ae7442234d63'),
                    'name': 'Salt - Celery',
                    'price': 198.14,
                    'quantity': 1
                },
                {
                    'id': ObjectId('6673e7c5fc13ae7442234d64'),
                    'name': 'Wine - Magnotta, Merlot Sr Vqa',
                    'price': 199.44,
                    'quantity': 3
                }
            ],
            'payed_by_bonuses': 45.11,
            'cost': 1415.07,
            'payment': 1415.07,
            'bonus_for_visit': 400,
            'statuses': [
                {
                    'status': 'OPEN',
                    'time': ISODate("2024-01-23T11:59:16.000Z")
                }
            ],
            'final_status': 'OPEN',
            'update_time': ISODate("2024-01-23T11:59:16.000Z")
        },
        {
            '_id': ObjectId('6673ef41fc13ae710e2348f5'),
            'restaurant': {'_id': ObjectId('6673e67bfc13ae74a9234abd')},
            'order_date': '08.05.2024',
            'client': {'_id': ObjectId('6673e2abfc13ae710e234884')},
            'ordered_dish': [
                {
                    'id': ObjectId('6673e7c5fc13ae7442234d60'),
                    'name': 'Squid Ink',
                    'price': 70.3,
                    'quantity': 1
                },
                {
                    'id': ObjectId('6673e7c5fc13ae7442234d61'),
                    'name': 'Nut - Pecan, Halves',
                    'price': 162.55,
                    'quantity': 4
                },
                {
                    'id': ObjectId('6673e7c5fc13ae7442234d5a'),
                    'name': 'Seedlings - Mix, Organic',
                    'price': 50.97,
                    'quantity': 4
                },
                {
                    'id': ObjectId('6673e7c5fc13ae7442234d62'),
                    'name': 'Sprite - 355 Ml',
                    'price': 166.52,
                    'quantity': 1
                }
            ],
            'payed_by_bonuses': 144.91,
            'cost': 1090.9,
            'payment': 1090.9,
            'bonus_for_visit': 4,
            'statuses': [
                {
                    'status': 'CLOSE',
                    'time': ISODate("2024-05-08T12:46:55.000Z")
                },
                {
                    'status': 'DELIVERING',
                    'time': ISODate("2024-05-08T11:46:55.000Z")
                },
                {
                    'status': 'COOKING',
                    'time': ISODate("2024-05-08T10:46:55.000Z")
                },
                {
                    'status': 'OPEN',
                    'time': ISODate("2024-05-08T09:46:55.000Z")
                }
            ],
            'final_status': 'CLOSE',
            'update_time': ISODate("2024-05-08T12:46:55.000Z")
        }
    ]
)