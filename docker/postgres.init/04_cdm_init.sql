create schema if not exists cdm;
set search_path to cdm;

create table if not exists deliveryman_income
(
    id                       bigserial primary key,
    deliveryman_id           varchar          not null,
    deliveryman_name         varchar          not null,
    year                     integer          not null,
    month                    integer          not null,
    orders_amount            integer          not null,
    orders_total_cost        double precision not null,
    rating                   double precision not null,
    company_commission       double precision not null,
    deliveryman_order_income double precision not null,
    tips                     double precision not null
);