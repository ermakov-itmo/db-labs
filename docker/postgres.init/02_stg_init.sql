create schema if not exists stg;
set search_path to stg;

create table if not exists mongo_clients
(
    id           bigserial primary key,
    obj_id       varchar     not null,
    obj_val      text        not null,
    when_updated timestamptz not null default now()
);

create table if not exists mongo_restaurants
(
    id           bigserial primary key,
    obj_id       varchar     not null,
    obj_val      text        not null,
    when_updated timestamptz not null default now()
);

create table if not exists mongo_orders
(
    id           bigserial primary key,
    obj_id       varchar     not null,
    obj_val      text        not null,
    when_updated timestamptz not null default now()
);

create table if not exists pg_category
(
    id           bigserial primary key,
    category_id  bigint           not null,
    name         VARCHAR(255)     not null,
    percent      double precision not null,
    min_payment  double precision not null,
    when_updated timestamptz      not null DEFAULT now()
);

create table if not exists pg_dish
(
    id           bigserial primary key,
    dish_id varchar not null,
    name         varchar(255)     not null,
    price        double precision not null,
    when_updated timestamptz      not null DEFAULT now()
);

create table if not exists pg_client
(
    id            bigserial primary key,
    client_id varchar not null,
    bonus_balance double precision not null,
    category_id   bigint           not null,
    when_updated  timestamptz      not null DEFAULT now()
);

create table if not exists api_deliveryman
(
    id           bigserial primary key,
    obj_id       varchar     not null,
    obj_val      text        not null,
    when_updated timestamptz not null default now()
);

create table if not exists api_delivery
(
    id           bigserial primary key,
    obj_id       varchar     not null,
    obj_val      text        not null,
    when_updated timestamptz not null default now()
);

create table if not exists settings
(
    id          bigserial primary key,
    setting_key varchar not null,
    settings    jsonb   not null
);

create table if not exists events
(
    event_id     bigserial primary key,
    id           bigint      not null,
    table_name   varchar     not null,
    action       varchar     not null,
    time         timestamptz not null,
    values       jsonb       not null,
    when_updated timestamptz not null default now()
);