CREATE SCHEMA IF NOT EXISTS src;
SET SEARCH_PATH TO src;

create table if not exists category
(
    category_id bigserial primary key,
    name        varchar(255)     not null,
    percent     double precision not null,
    min_payment double precision not null
);

create table if not exists client
(
    client_id     varchar primary key,
    bonus_balance double precision not null,
    category_id   bigint           not null references category (category_id)
);

create table if not exists dish
(
    dish_id varchar primary key,
    name    varchar(255)     not null,
    price   double precision not null
);

create table payment
(
    payment_id  bigserial primary key,
    client_id   varchar          not null references client (client_id),
    dish_id     varchar          not null references dish (dish_id),
    dish_amount integer          not null,
    order_id    varchar          not null,
    order_time  timestamptz      not null,
    order_sum   double precision not null,
    tips        double precision not null
);

create table if not exists logs
(
    id         bigserial primary key,
    table_name varchar(255) not null,
    action     varchar(255) not null,
    time       timestamptz  not null,
    values     jsonb        not null
);

create or replace function log_changes()
    returns trigger as
$$
begin
    IF (tg_op = 'INSERT') then
        insert into logs(table_name, action, time, values)
        values (tg_table_name, 'insert', now(), row_to_json(new));
    ELSE
        insert into logs(table_name, action, time, values)
        values (tg_table_name, 'update', now(), row_to_json(new));
    end if;
    return new;
end;
$$
    language plpgsql;

create trigger log_category_changes
    after insert or update
    on category
    for each row
execute procedure log_changes();

create trigger log_client_changes
    after insert or update
    on client
    for each row
execute procedure log_changes();

create trigger log_dish_changes
    after insert or update
    on dish
    for each row
execute procedure log_changes();

create trigger log_payment_changes
    after insert or update
    on payment
    for each row
execute procedure log_changes();

insert into category(name, percent, min_payment)
values ('user category #1', 10, 1000),
       ('user category #2', 20, 10000),
       ('user category #3', 30, 20000),
       ('user category #4', 40, 30000),
       ('user category #5', 50, 50000);

insert into client(client_id, bonus_balance, category_id)
values ('6673e2abfc13ae710e234882', 1005, 1),
       ('6673e2abfc13ae710e234883', 35752, 4),
       ('6673e2abfc13ae710e234884', 21071, 3);

insert into dish(dish_id, name, price)
values ('6673e7c5fc13ae7442234d5b', 'Beef - Bones, Cut - Up', 87.17),
       ('6673e7c5fc13ae7442234d5c', 'Pasta - Cannelloni, Sheets, Fresh', 196.14),
       ('6673e7c5fc13ae7442234d5d', 'Placemat - Scallop, White', 144.06),
       ('6673e7c5fc13ae7442234d5e', 'Wine - Jackson Triggs Okonagan', 85.46),
       ('6673e7c5fc13ae7442234d5f', 'Island Oasis - Lemonade', 115.88),

       ('6673e7c5fc13ae7442234d5a', 'Seedlings - Mix, Organic', 50.97),
       ('6673e7c5fc13ae7442234d60', 'Squid Ink', 70.3),
       ('6673e7c5fc13ae7442234d61', 'Nut - Pecan, Halves', 162.55),
       ('6673e7c5fc13ae7442234d62', 'Sprite - 355 Ml', 166.52),
       ('6673e7c5fc13ae7442234d63', 'Salt - Celery', 198.14),
       ('6673e7c5fc13ae7442234d64', 'Wine - Magnotta, Merlot Sr Vqa', 199.44),

       ('6673e7c5fc13ae7442234d65', 'Tray - Foam, Square 4 - S', 31.18),
       ('6673e7c5fc13ae7442234d66', 'Cheese - St. Andre', 70.19),
       ('6673e7c5fc13ae7442234d67', 'Water - San Pellegrino', 94.68),

       ('6673e7c5fc13ae7442234d68', 'Chicken - Breast, 5 - 7 Oz', 77.94),
       ('6673e7c5fc13ae7442234d69', 'Stock - Chicken, White', 50.45),
       ('6673e7c5fc13ae7442234d6a', 'Longos - Chicken Cordon Bleu', 41.58),
       ('6673e7c5fc13ae7442234d6b', 'Pork - Back, Long Cut, Boneless', 96.27),
       ('6673e7c5fc13ae7442234d6c', 'Lighter - Bbq', 24.07),
       ('6673e7c5fc13ae7442234d6d', 'Guinea Fowl', 88.79),
       ('6673e7c5fc13ae7442234d6e', 'Water - Aquafina Vitamin', 198.86),

       ('6673e7c5fc13ae7442234d6f', 'Pork Ham Prager', 27.25),
       ('6673e7c5fc13ae7442234d70', 'Chicken - Leg / Back Attach', 51.55),
       ('6673e7c5fc13ae7442234d71', 'Oyster - In Shell', 85.65),
       ('6673e7c5fc13ae7442234d72', 'Bamboo Shoots - Sliced', 167.35),
       ('6673e7c5fc13ae7442234d73', 'Wine - Piper Heidsieck Brut', 80.38),
       ('6673e7c5fc13ae7442234d74', 'Pastry - Plain Baked Croissant', 166.84),
       ('6673e7c5fc13ae7442234d75', 'Octopus - Baby, Cleaned', 75.19),
       ('6673e7c5fc13ae7442234d76', 'Pork - Side Ribs', 118.55),
       ('6673e7c5fc13ae7442234d77', 'Cheese - Fontina', 52.13);

insert into payment(client_id, dish_id, dish_amount, order_id, order_time, order_sum, tips)
values ('6673e2abfc13ae710e234882', '6673e7c5fc13ae7442234d5b', 3, '6673ef41fc13ae710e2348f0',
        '2024-04-24T15:17:52.000Z', 548.31, 100),
       ('6673e2abfc13ae710e234882', '6673e7c5fc13ae7442234d5e', 2, '6673ef41fc13ae710e2348f0',
        '2024-04-24T15:17:52.000Z', 548.31, 100),
       ('6673e2abfc13ae710e234882', '6673e7c5fc13ae7442234d5f', 1, '6673ef41fc13ae710e2348f0',
        '2024-04-24T15:17:52.000Z', 548.31, 100),
       ('6673e2abfc13ae710e234883', '6673e7c5fc13ae7442234d5e', 1, '6673ef41fc13ae710e2348f1',
        '2024-05-15T19:41:20.000Z', 85.46, 10),
       ('6673e2abfc13ae710e234884', '6673e7c5fc13ae7442234d5d', 1, '6673ef41fc13ae710e2348f2',
        '2024-03-15T17:47:23.000Z', 314.98, 50),
       ('6673e2abfc13ae710e234884', '6673e7c5fc13ae7442234d5e', 2, '6673ef41fc13ae710e2348f2',
        '2024-03-15T17:47:23.000Z', 314.98, 50),
       ('6673e2abfc13ae710e234882', '6673e7c5fc13ae7442234d5a', 4, '6673ef41fc13ae710e2348f3',
        '2024-03-11T17:47:23.000Z', 1597.04, 250),
       ('6673e2abfc13ae710e234882', '6673e7c5fc13ae7442234d60', 2, '6673ef41fc13ae710e2348f3',
        '2024-03-11T17:47:23.000Z', 1597.04, 250),
       ('6673e2abfc13ae710e234882', '6673e7c5fc13ae7442234d61', 2, '6673ef41fc13ae710e2348f3',
        '2024-03-11T17:47:23.000Z', 1597.04, 250),
       ('6673e2abfc13ae710e234882', '6673e7c5fc13ae7442234d62', 2, '6673ef41fc13ae710e2348f3',
        '2024-03-11T17:47:23.000Z', 1597.04, 250),
       ('6673e2abfc13ae710e234882', '6673e7c5fc13ae7442234d63', 3, '6673ef41fc13ae710e2348f3',
        '2024-03-11T17:47:23.000Z', 1597.04, 250),
       ('6673e2abfc13ae710e234883', '6673e7c5fc13ae7442234d5a', 3, '6673ef41fc13ae710e2348f4',
        '2024-01-23T11:59:16.000Z', 1415.07, 150),
       ('6673e2abfc13ae710e234883', '6673e7c5fc13ae7442234d60', 2, '6673ef41fc13ae710e2348f4',
        '2024-01-23T11:59:16.000Z', 1415.07, 150),
       ('6673e2abfc13ae710e234883', '6673e7c5fc13ae7442234d61', 2, '6673ef41fc13ae710e2348f4',
        '2024-01-23T11:59:16.000Z', 1415.07, 150),
       ('6673e2abfc13ae710e234883', '6673e7c5fc13ae7442234d63', 1, '6673ef41fc13ae710e2348f4',
        '2024-01-23T11:59:16.000Z', 1415.07, 150),
       ('6673e2abfc13ae710e234883', '6673e7c5fc13ae7442234d64', 3, '6673ef41fc13ae710e2348f4',
        '2024-01-23T11:59:16.000Z', 1415.07, 150),
       ('6673e2abfc13ae710e234884', '6673e7c5fc13ae7442234d60', 1, '6673ef41fc13ae710e2348f5',
        '2024-05-08T12:46:55.000Z', 1090.9, 100),
       ('6673e2abfc13ae710e234884', '6673e7c5fc13ae7442234d61', 4, '6673ef41fc13ae710e2348f5',
        '2024-05-08T12:46:55.000Z', 1090.9, 100),
       ('6673e2abfc13ae710e234884', '6673e7c5fc13ae7442234d5a', 4, '6673ef41fc13ae710e2348f5',
        '2024-05-08T12:46:55.000Z', 1090.9, 100),
       ('6673e2abfc13ae710e234884', '6673e7c5fc13ae7442234d62', 1, '6673ef41fc13ae710e2348f5',
        '2024-05-08T12:46:55.000Z', 1090.9, 100);