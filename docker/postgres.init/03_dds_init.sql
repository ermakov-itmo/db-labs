create schema if not exists dds;
set search_path to dds;

create table if not exists settings
(
    id          bigserial primary key,
    setting_key varchar not null,
    settings    jsonb   not null
);

create table if not exists dm_deliveryman
(
    id             bigserial primary key,
    deliveryman_id varchar not null unique,
    name           varchar not null
);

create table if not exists dm_date
(
    id    bigserial primary key,
    date  date     not null,
    year  smallint not null check ( year > 2022 ),
    month smallint not null check ( month >= 1 and month <= 12 ),
    day   smallint not null check ( day >= 1 and day <= 31 )
);

create table if not exists dm_orders
(
    id             bigserial primary key,
    order_id       varchar          not null unique,
    date_id        bigint           not null references dm_date (id),
    final_status   varchar          not null,
    payed_by_bonus double precision not null,
    cost           double precision not null,
    payment        double precision not null
);

create table if not exists dm_delivery
(
    id             bigserial primary key,
    delivery_id    varchar          not null unique,
    deliveryman_id bigint           not null references dm_deliveryman (id),
    order_id       bigint           not null references dm_orders (id),
    rating         smallint         not null,
    tips           double precision not null
);