import pendulum
from airflow.decorators import dag, task

import stg_mongo_logic


@dag(
    schedule_interval='0/10 * * * *',
    start_date=pendulum.datetime(2024, 5, 30, tz="UTC"),
    catchup=False,
    tags=['pg', 'stg'],
    is_paused_upon_creation=True
)
def load_mongo_to_stg():
    @task
    def load_clients():
        stg_mongo_logic.load_mongo_data('Clients', 'mongo_clients')

    @task
    def load_restaurants():
        stg_mongo_logic.load_mongo_data('Restaurant', 'mongo_restaurants')

    @task
    def load_orders():
        stg_mongo_logic.load_mongo_data('Orders', 'mongo_orders')

    client_task = load_clients()
    restaurant_task = load_restaurants()
    orders_task = load_orders()

    [client_task, restaurant_task, orders_task]


mongo_stg_dag = load_mongo_to_stg()
