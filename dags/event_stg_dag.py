import pendulum
from airflow.decorators import dag, task

import stg_event_logic


@dag(
    schedule_interval='0/10 * * * *',
    start_date=pendulum.datetime(2024, 5, 30, tz="UTC"),
    catchup=False,
    tags=['pg', 'stg'],
    is_paused_upon_creation=True
)
def load_logs_to_stg():
    @task
    def load_logs():
        stg_event_logic.load_event_data()

    logs_task = load_logs()

    [logs_task]


logs_stg_dag = load_logs_to_stg()
