import json
import pendulum
import requests
from airflow.models import Variable
from sqlalchemy import MetaData, select, insert, update
from sqlalchemy.orm import sessionmaker

import db_utils

API_URL = Variable.get('API_URL')
BATCH_SIZE = 100


def get_mens(offset):
    uri = f'{API_URL}/mans/{BATCH_SIZE}/{offset}'

    response = requests.get(uri)
    if response.status_code == 200:
        return response.json()
    else:
        raise Exception('Failed to retrieve mens.')


def get_deliveries(offset):
    uri = f'{API_URL}/delivery/{BATCH_SIZE}/{offset}'
    response = requests.get(uri)
    if response.status_code == 200:
        return response.json()
    else:
        raise Exception('Failed to retrieve deliveries.')


def load_mens_data():
    pg_engine = db_utils.get_pg_engine()
    stg_session_mk = sessionmaker(bind=pg_engine)
    stg_session = stg_session_mk()

    stg_metadata = MetaData()
    stg_metadata.reflect(bind=pg_engine, schema='stg')

    stg_mans = stg_metadata.tables['stg.api_deliveryman']
    # stg_delivery = stg_metadata.tables['stg.api_delivery']
    stg_settings = stg_metadata.tables['stg.settings']

    query = select(stg_settings).where(stg_settings.c.setting_key == 'api_deliveryman_last_offset')
    result = stg_session.execute(query).fetchone()

    if result:
        last_offset = result['settings']['offset']
    else:
        last_offset = 0
        initial_settings = insert(stg_settings).values(
            setting_key='api_deliveryman_last_offset',
            settings={'offset': last_offset}
        )
        stg_session.execute(initial_settings)
        stg_session.commit()

    mans = get_mens(last_offset)

    saved = 0
    for row in mans:
        id = row['_id']
        name = row['name']

        exist_data = stg_session.execute(select(stg_mans).where(stg_mans.c.obj_id == id)).fetchone()

        if exist_data:
            update_req = update(stg_mans).where(stg_mans.c.obj_id == id).values(
                obj_val=name,
                when_updated=pendulum.now().to_iso8601_string()
            )
            stg_session.execute(update_req)
        else:
            insert_req = insert(stg_mans).values(
                obj_id=id,
                obj_val=name,
                when_updated=pendulum.now().to_iso8601_string()
            )
            stg_session.execute(insert_req)
        saved += 1
    update_settings = update(stg_settings).where(stg_settings.c.setting_key == 'api_deliveryman_last_offset').values(
        settings={'offset': last_offset + saved}
    )
    stg_session.execute(update_settings)
    stg_session.commit()
    stg_session.close()


def load_deliveries_data():
    pg_engine = db_utils.get_pg_engine()
    stg_session_mk = sessionmaker(bind=pg_engine)
    stg_session = stg_session_mk()

    stg_metadata = MetaData()
    stg_metadata.reflect(bind=pg_engine, schema='stg')

    stg_delivery = stg_metadata.tables['stg.api_delivery']
    stg_settings = stg_metadata.tables['stg.settings']

    query = select(stg_settings).where(stg_settings.c.setting_key == 'api_delivery_last_offset')
    result = stg_session.execute(query).fetchone()

    if result:
        last_offset = result['settings']['offset']
    else:
        last_offset = 0
        initial_settings = insert(stg_settings).values(
            setting_key='api_delivery_last_offset',
            settings={'offset': last_offset}
        )
        stg_session.execute(initial_settings)
        stg_session.commit()

    deliveries = get_deliveries(last_offset)

    saved = 0
    for row in deliveries:
        id = row['delivery_id']
        json_data = json.dumps(row, default=str, ensure_ascii=False)

        exist_data = stg_session.execute(select(stg_delivery).where(stg_delivery.c.obj_id == id)).fetchone()

        if exist_data:
            update_req = update(stg_delivery).where(stg_delivery.c.obj_id == id).values(
                obj_val=json_data,
                when_updated=pendulum.now().to_iso8601_string()
            )
            stg_session.execute(update_req)
        else:
            insert_req = insert(stg_delivery).values(
                obj_id=id,
                obj_val=json_data,
                when_updated=pendulum.now().to_iso8601_string()
            )
            stg_session.execute(insert_req)
        saved += 1
    update_settings = update(stg_settings).where(stg_settings.c.setting_key == 'api_delivery_last_offset').values(
        settings={'offset': last_offset + saved}
    )
    stg_session.execute(update_settings)
    stg_session.commit()
    stg_session.close()
