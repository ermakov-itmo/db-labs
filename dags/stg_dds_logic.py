import json
import pendulum
from sqlalchemy import MetaData, select, insert, update
from sqlalchemy.orm import sessionmaker

import db_utils


def load_deliveryman_data():
    pg_engine = db_utils.get_pg_engine()
    stg_session_mk = sessionmaker(bind=pg_engine)
    dds_session_mk = sessionmaker(bind=pg_engine)

    stg_session = stg_session_mk()
    dds_session = dds_session_mk()

    stg_metadata = MetaData()
    stg_metadata.reflect(bind=pg_engine, schema='stg')
    dds_metadata = MetaData()
    dds_metadata.reflect(bind=pg_engine, schema='dds')

    stg_table = stg_metadata.tables['stg.api_deliveryman']
    dds_table = dds_metadata.tables['dds.dm_deliveryman']
    settings = dds_metadata.tables['dds.settings']

    query = select(settings).where(settings.c.setting_key == 'deliveryman_last_update')
    result = dds_session.execute(query).fetchone()

    if result:
        last_update = result['settings']['last_update']
    else:
        last_update = '1970-01-01T00:00:00Z'
        initial_settings = insert(settings).values(
            setting_key='deliveryman_last_update',
            settings={'last_update': last_update}
        )
        stg_session.execute(initial_settings)
        stg_session.commit()

    current_time = pendulum.now()
    parsed_time = pendulum.parse(last_update)
    query = select(stg_table).where(stg_table.c.when_updated.between(parsed_time, current_time))
    rows = stg_session.execute(query).fetchall()

    for row in rows:
        obj_id = row['obj_id']
        obj_name = row['obj_val']

        exist_data = dds_session.execute(select(dds_table).where(dds_table.c.deliveryman_id == obj_id)).fetchone()
        if exist_data:
            update_req = update(dds_table).where(dds_table.c.deliveryman_id == obj_id).values(name=obj_name)
            dds_session.execute(update_req)
        else:
            insert_req = insert(dds_table).values(deliveryman_id=obj_id, name=obj_name)
            dds_session.execute(insert_req)

    update_settings = update(settings).where(settings.c.setting_key == 'deliveryman_last_update').values(
        settings={'last_update': current_time.to_iso8601_string()}
    )
    dds_session.execute(update_settings)
    dds_session.commit()
    dds_session.close()
    stg_session.close()


def load_orders_data():
    pg_engine = db_utils.get_pg_engine()
    stg_session_mk = sessionmaker(bind=pg_engine)
    dds_session_mk = sessionmaker(bind=pg_engine)

    stg_session = stg_session_mk()
    dds_session = dds_session_mk()

    stg_metadata = MetaData()
    stg_metadata.reflect(bind=pg_engine, schema='stg')
    dds_metadata = MetaData()
    dds_metadata.reflect(bind=pg_engine, schema='dds')

    stg_table = stg_metadata.tables['stg.mongo_orders']
    dds_table = dds_metadata.tables['dds.dm_orders']
    time_table = dds_metadata.tables['dds.dm_date']
    settings = dds_metadata.tables['dds.settings']

    query = select(settings).where(settings.c.setting_key == 'orders_last_update')
    result = dds_session.execute(query).fetchone()

    if result:
        last_update = result['settings']['last_update']
    else:
        last_update = '1970-01-01T00:00:00Z'
        initial_settings = insert(settings).values(
            setting_key='orders_last_update',
            settings={'last_update': last_update}
        )
        stg_session.execute(initial_settings)
        stg_session.commit()

    current_time = pendulum.now()
    parsed_time = pendulum.parse(last_update)
    query = select(stg_table).where(stg_table.c.when_updated.between(parsed_time, current_time))
    rows = stg_session.execute(query).fetchall()

    for row in rows:
        obj_id = row['obj_id']
        json_value = row['obj_val']
        parsed_data = json.loads(json_value)

        final_status = parsed_data['final_status']
        statuses_list = parsed_data['statuses']
        final_status_time = [st['time'] for st in statuses_list if st['status'] == final_status][0]
        parsed_time = pendulum.parse(final_status_time)
        status_date = parsed_time.date()

        dm_date = dds_session.execute(select(time_table).where(time_table.c.date == status_date)).fetchone()
        if dm_date:
            dm_date_id = dm_date['id']
        else:
            dm_insert_req = insert(time_table).values(
                date=status_date,
                year=status_date.year,
                month=status_date.month,
                day=status_date.day
            ).returning(time_table.c.id)
            dm_date_id = dds_session.execute(dm_insert_req).fetchone()[0]

        payed_by_bonus = parsed_data['payed_by_bonuses']
        cost = parsed_data['cost']
        payment = parsed_data['payment']

        exist_data = dds_session.execute(select(dds_table).where(dds_table.c.order_id == obj_id)).fetchone()
        if exist_data:
            update_req = update(dds_table).where(dds_table.c.order_id == obj_id).values(
                date_id=dm_date_id,
                final_status=final_status,
                payed_by_bonus=payed_by_bonus,
                cost=cost,
                payment=payment
            )
            dds_session.execute(update_req)
        else:
            insert_req = insert(dds_table).values(
                order_id=obj_id,
                date_id=dm_date_id,
                final_status=final_status,
                payed_by_bonus=payed_by_bonus,
                cost=cost,
                payment=payment
            )
            dds_session.execute(insert_req)

    update_settings = update(settings).where(settings.c.setting_key == 'orders_last_update').values(
        settings={'last_update': current_time.to_iso8601_string()}
    )
    dds_session.execute(update_settings)

    dds_session.commit()
    dds_session.close()
    stg_session.close()


def load_delivery_data():
    pg_engine = db_utils.get_pg_engine()
    stg_session_mk = sessionmaker(bind=pg_engine)
    dds_session_mk = sessionmaker(bind=pg_engine)

    stg_session = stg_session_mk()
    dds_session = dds_session_mk()

    stg_metadata = MetaData()
    stg_metadata.reflect(bind=pg_engine, schema='stg')
    dds_metadata = MetaData()
    dds_metadata.reflect(bind=pg_engine, schema='dds')

    stg_table = stg_metadata.tables['stg.api_delivery']
    dds_table = dds_metadata.tables['dds.dm_delivery']
    settings = dds_metadata.tables['dds.settings']
    order_table = dds_metadata.tables['dds.dm_orders']
    men_table = dds_metadata.tables['dds.dm_deliveryman']

    query = select(settings).where(settings.c.setting_key == 'delivery_last_update')
    result = dds_session.execute(query).fetchone()

    if result:
        last_update = result['settings']['last_update']
    else:
        last_update = '1970-01-01T00:00:00Z'
        initial_settings = insert(settings).values(
            setting_key='delivery_last_update',
            settings={'last_update': last_update}
        )
        stg_session.execute(initial_settings)
        stg_session.commit()

    current_time = pendulum.now()
    parsed_time = pendulum.parse(last_update)
    query = select(stg_table).where(stg_table.c.when_updated.between(parsed_time, current_time))
    rows = stg_session.execute(query).fetchall()

    for row in rows:
        obj_id = row['obj_id']
        json_value = row['obj_val']
        parsed_data = json.loads(json_value)

        tips = parsed_data['tips']
        rating = parsed_data['rating']

        order_raw_id = parsed_data['order_id']
        order_req = select(order_table).where(order_table.c.order_id == order_raw_id)
        order_id = dds_session.execute(order_req).fetchone()['id']

        deliveryman_raw_id = parsed_data['deliveryman_id']
        man_req = select(men_table).where(men_table.c.deliveryman_id == deliveryman_raw_id)
        deliveryman_id = dds_session.execute(man_req).fetchone()['id']

        exist_data = dds_session.execute(select(dds_table).where(dds_table.c.delivery_id == obj_id)).fetchone()
        if exist_data:
            update_req = update(dds_table).where(dds_table.c.delivery_id == obj_id).values(
                deliveryman_id=deliveryman_id,
                order_id=order_id,
                rating=rating,
                tips=tips
            )
            dds_session.execute(update_req)
        else:
            insert_req = insert(dds_table).values(
                delivery_id=obj_id,
                deliveryman_id=deliveryman_id,
                order_id=order_id,
                rating=rating,
                tips=tips
            )
            dds_session.execute(insert_req)

    update_settings = update(settings).where(settings.c.setting_key == 'delivery_last_update').values(
        settings={'last_update': current_time.to_iso8601_string()}
    )
    dds_session.execute(update_settings)

    dds_session.commit()
    dds_session.close()
    stg_session.close()
