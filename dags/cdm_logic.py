import pendulum
from datetime import timedelta
from pendulum import DateTime
from sqlalchemy import MetaData, select, insert, update, text
from sqlalchemy.orm import sessionmaker

import db_utils


def start_date(cur_date: DateTime):
    if cur_date.day < 21:
        if cur_date.month == 1:
            return pendulum.date(cur_date.year - 1, 12, 21)
        else:
            return pendulum.date(cur_date.year, cur_date.month - 1, 21)
    else:
        return pendulum.date(cur_date.year, cur_date.month, 21)


def stop_date(cur_date: DateTime):
    if cur_date.day < 21:
        return pendulum.date(cur_date.year, cur_date.month, 21)
    else:
        if cur_date.month == 12:
            return pendulum.date(cur_date.year + 1, 1, 21)
        else:
            return pendulum.date(cur_date.year, cur_date.month + 1, 21)


def get_date_in_period(cur_date: DateTime):
    if cur_date.day < 21:
        return cur_date + timedelta(days=-30)
    else:
        return cur_date


def build_data():
    current_date = pendulum.date(2024, 6, 15)  ## only for test
    ##current_date = pendulum.now()
    start = start_date(get_date_in_period(current_date))
    finish = stop_date(get_date_in_period(current_date))

    print(start)
    print(finish)

    pg_engine = db_utils.get_pg_engine()
    cdm_session_mk = sessionmaker(bind=pg_engine)
    dds_session_mk = sessionmaker(bind=pg_engine)
    dds_session = dds_session_mk()
    cdm_session = cdm_session_mk()

    dds_metadata = MetaData()
    dds_metadata.reflect(bind=pg_engine, schema='dds')
    cdm_metadata = MetaData()
    cdm_metadata.reflect(bind=pg_engine, schema='cdm')

    cdm_table = cdm_metadata.tables['cdm.deliveryman_income']
    date_table = dds_metadata.tables['dds.dm_date']
    delivery_table = dds_metadata.tables['dds.dm_delivery']
    deliveryman_table = dds_metadata.tables['dds.dm_deliveryman']
    orders_table = dds_metadata.tables['dds.dm_orders']

    request = select(date_table).where(date_table.c.date.between(start, finish))
    dates_rows = dds_session.execute(request).fetchall()
    dates_ids = [row['id'] for row in dates_rows]
    print(dates_ids)

    orders_request = select(orders_table).where(
        orders_table.c.final_status == 'CLOSE',
        orders_table.c.date_id.in_(dates_ids)
    )
    orders_rows = dds_session.execute(orders_request).fetchall()
    orders_ids = [row['id'] for row in orders_rows]
    print(orders_ids)

    delivery_request = select(delivery_table).where(delivery_table.c.order_id.in_(orders_ids))
    delivery_rows = dds_session.execute(delivery_request).fetchall()
    delivery_ids = [row['id'] for row in delivery_rows]
    delivery_men_ids = [row['deliveryman_id'] for row in delivery_rows]
    print(delivery_ids)
    print(delivery_men_ids)

    mens_request = select(deliveryman_table).where(deliveryman_table.c.id.in_(delivery_men_ids))
    print(mens_request)
    mens_rows = dds_session.execute(mens_request).fetchall()

    for men in mens_rows:
        raw_id = men['deliveryman_id']
        name = men['name']
        year = current_date.year
        month = current_date.month

        agg_query = select([
            text('COUNT(dm_orders.id) as total_orders'),
            text('SUM(dm_orders.cost) as total_cost'),
            text('AVG(dm_delivery.rating) as rating'),
            text('SUM(dm_delivery.tips) as tips')
        ]).select_from(
            delivery_table.join(orders_table, delivery_table.c.order_id == orders_table.c.id)
        ).where(
            delivery_table.c.deliveryman_id == men['id'],
            delivery_table.c.id.in_(delivery_ids)
        )
        result = dds_session.execute(agg_query).fetchone()

        exist_req = select(cdm_table).where(
            cdm_table.c.deliveryman_id == raw_id,
            cdm_table.c.year == year,
            cdm_table.c.month == month
        )

        exist_data = cdm_session.execute(exist_req).fetchone()

        rating = result['rating']
        total_cost = result['total_cost']
        if rating < 8:
            payment = max(400, total_cost * 0.05)
        else:
            payment = min(1000, total_cost * 0.1)

        if exist_data:
            update_req = update(cdm_table).where(
                cdm_table.c.deliveryman_id == raw_id,
                cdm_table.c.year == year,
                cdm_table.c.month == month
            ).values(
                deliveryman_name=name,
                orders_amount=result['total_orders'],
                orders_total_cost=total_cost,
                rating=rating,
                tips=result['tips'],
                company_commission=total_cost * 0.5,
                deliveryman_order_income=payment
            )
            cdm_session.execute(update_req)
        else:
            insert_req = insert(cdm_table).values(
                deliveryman_id=raw_id,
                year=year,
                month=month,
                deliveryman_name=name,
                orders_amount=result['total_orders'],
                orders_total_cost=total_cost,
                rating=rating,
                tips=result['tips'],
                company_commission=total_cost * 0.5,
                deliveryman_order_income=payment
            )
            cdm_session.execute(insert_req)

    cdm_session.commit()
    cdm_session.close()
    dds_session.close()
