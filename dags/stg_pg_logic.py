import pendulum
from sqlalchemy import MetaData, select, insert, update
from sqlalchemy.orm import sessionmaker

import db_utils

BATCH_SIZE = 10


def load_pg_data(src_table_name, table_name):
    pg_engine = db_utils.get_pg_engine()
    src_session_mk = sessionmaker(bind=pg_engine)
    stg_session_mk = sessionmaker(bind=pg_engine)

    src_session = src_session_mk()
    stg_session = stg_session_mk()

    src_metadata = MetaData()
    src_metadata.reflect(bind=pg_engine, schema='src')
    stg_metadata = MetaData()
    stg_metadata.reflect(bind=pg_engine, schema='stg')

    src_table = src_metadata.tables[f'src.{src_table_name}']
    stg_table = stg_metadata.tables[f'stg.{table_name}']
    stg_settings = stg_metadata.tables['stg.settings']

    query = select(stg_settings).where(stg_settings.c.setting_key == f'{src_table_name}_last_update')
    result = stg_session.execute(query).fetchone()

    if result:
        last_update = result['settings']['last_update']
    else:
        last_update = '1970-01-01T00:00:00Z'
        initial_settings = insert(stg_settings).values(
            setting_key=f'{src_table_name}_last_update',
            settings={'last_update': last_update}
        )
        stg_session.execute(initial_settings)
        stg_session.commit()

    offset = 0
    key_id = getattr(src_table.c, src_table_name + "_id")
    query = select(src_table).order_by(key_id).offset(offset).limit(BATCH_SIZE)
    src_rows = src_session.execute(query).fetchall()
    current_time = pendulum.now()

    while True:
        move = 0
        for row in src_rows:
            pg_id = row[key_id]
            exist_data = stg_session.execute(
                select(stg_table).where(stg_table.c[src_table_name + "_id"] == pg_id)
            ).fetchone()

            if exist_data:
                update_req = update(stg_table).where(stg_table.c[src_table_name + "_id"] == pg_id).values(
                    **row,
                    when_updated=current_time.to_iso8601_string()
                )
                stg_session.execute(update_req)
            else:
                insert_req = insert(stg_table).values(
                    **row,
                    when_updated=current_time.to_iso8601_string()
                )
                stg_session.execute(insert_req)
            move = move + 1

        if move == 0:
            break

        offset = offset + BATCH_SIZE
        query = select(src_table).order_by(key_id).offset(offset).limit(BATCH_SIZE)
        src_rows = src_session.execute(query).fetchall()

    update_settings_req = (update(stg_settings).where(stg_settings.c.setting_key == f'{src_table_name}_last_update')
                           .values(settings={'last_update': current_time.to_iso8601_string()}))

    stg_session.execute(update_settings_req)
    stg_session.commit()
    stg_session.close()
    src_session.close()
