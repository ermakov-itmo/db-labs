from airflow.models import Variable
from pymongo import MongoClient
from pymongo.errors import ConnectionFailure
from sqlalchemy import create_engine

MONGO_URI = Variable.get('MONGO_URL')
PG_URI = Variable.get('PG_URL')


def get_mongo_client():
    try:
        client = MongoClient(MONGO_URI)
        client.admin.command('ping')
        return client
    except ConnectionFailure as e:
        print(e)
        raise Exception(e)


def get_pg_engine():
    return create_engine(PG_URI)
