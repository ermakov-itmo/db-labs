import pendulum
from sqlalchemy import MetaData, select, insert, update
from sqlalchemy.orm import sessionmaker

import db_utils

BATCH_SIZE = 10


def load_event_data():
    pg_engine = db_utils.get_pg_engine()
    src_session_mk = sessionmaker(bind=pg_engine)
    stg_session_mk = sessionmaker(bind=pg_engine)

    src_session = src_session_mk()
    stg_session = stg_session_mk()

    src_metadata = MetaData()
    src_metadata.reflect(bind=pg_engine, schema='src')
    stg_metadata = MetaData()
    stg_metadata.reflect(bind=pg_engine, schema='stg')

    src_table = src_metadata.tables['src.logs']
    stg_table = stg_metadata.tables['stg.events']
    stg_settings = stg_metadata.tables['stg.settings']

    query = select(stg_settings).where(stg_settings.c.setting_key == 'logs_last_saved')
    result = stg_session.execute(query).fetchone()

    if result:
        last_saved = result['settings']['last_saved']
    else:
        last_saved = 0
        initial_settings = insert(stg_settings).values(
            setting_key='logs_last_saved',
            settings={'last_saved': last_saved}
        )
        stg_session.execute(initial_settings)
        stg_session.commit()

    query = select(src_table).where(src_table.c.id > last_saved).order_by(src_table.c.id).limit(BATCH_SIZE)
    src_rows = src_session.execute(query).fetchall()

    saved = 0
    for row in src_rows:
        pg_id = row['id']
        insert_req = insert(stg_table).values(
            **row,
            when_updated=pendulum.now().to_iso8601_string()
        )
        stg_session.execute(insert_req)
        saved = pg_id

    update_settings_req = (update(stg_settings).where(stg_settings.c.setting_key == 'logs_last_saved')
                           .values(settings={'last_saved': saved}))
    stg_session.execute(update_settings_req)
    stg_session.commit()
    stg_session.close()
    src_session.close()
