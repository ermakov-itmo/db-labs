import pendulum
from airflow.decorators import dag, task

import stg_pg_logic


@dag(
    schedule_interval='0/10 * * * *',
    start_date=pendulum.datetime(2024, 5, 30, tz="UTC"),
    catchup=False,
    tags=['pg', 'stg'],
    is_paused_upon_creation=True
)
def load_pg_to_stg():
    @task
    def load_category():
        stg_pg_logic.load_pg_data('category', 'pg_category')

    @task
    def load_dish():
        stg_pg_logic.load_pg_data('dish', 'pg_dish')

    @task
    def load_client():
        stg_pg_logic.load_pg_data('client', 'pg_client')

    category_task = load_category()
    dish_task = load_dish()
    client_task = load_client()

    [category_task, dish_task, client_task]


postgres_stg_dag = load_pg_to_stg()
