import json
import pendulum
from sqlalchemy import MetaData, select, insert, update
from sqlalchemy.orm import sessionmaker

import db_utils

BATCH_SIZE = 10


def load_mongo_data(collection, table_name):
    client = db_utils.get_mongo_client()
    mongo_db = client['db-storage']

    pg_engine = db_utils.get_pg_engine()
    pg_session_mk = sessionmaker(bind=pg_engine)
    pg_metadata = MetaData()
    pg_metadata.reflect(bind=pg_engine, schema='stg')
    pg_session = pg_session_mk()
    pg_table = pg_metadata.tables[f'stg.{table_name}']
    pg_settings = pg_metadata.tables['stg.settings']

    query = select(pg_settings).where(pg_settings.c.setting_key == f'{table_name}_last_update')
    result = pg_session.execute(query).fetchone()

    start_time = None
    if result:
        last_update = result['settings']['last_update']
        start_time = last_update
    else:
        last_update = '1970-01-01T00:00:00Z'
        initial_settings = insert(pg_settings).values(
            setting_key=f'{table_name}_last_update',
            settings={'last_update': last_update}
        )
        pg_session.execute(initial_settings)
        pg_session.commit()
        start_time = last_update

    mng_collection = mongo_db[collection]
    current_time = pendulum.now()
    converted_time = pendulum.parse(start_time)
    mng_new_data = mng_collection.find({'update_time': {'$gte': converted_time, '$lte': current_time}})

    for mng_doc in mng_new_data:
        mng_id = str(mng_doc['_id'])
        mng_doc.pop('_id')
        mng_json = json.dumps(mng_doc, default=str, ensure_ascii=False)

        exist_data = pg_session.execute(select(pg_table).where(pg_table.c.obj_id == mng_id)).fetchone()
        if exist_data:
            update_req = update(pg_table).where(pg_table.c.obj_id == mng_id).value(
                obj_val=mng_json,
                when_updated=current_time.to_iso8601_string()
            )
            pg_session.execute(update_req)
        else:
            insert_req = insert(pg_table).values(
                obj_id=f'{mng_id}',
                obj_val=mng_json,
                when_updated=current_time.to_iso8601_string()
            )
            pg_session.execute(insert_req)

    update_setting_req = update(pg_settings).where(pg_settings.c.setting_key == f'{table_name}_last_update').values(
        settings={'last_update': current_time.to_iso8601_string()}
    )
    pg_session.execute(update_setting_req)
    pg_session.commit()
    pg_session.close()
