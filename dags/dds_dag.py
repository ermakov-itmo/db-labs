import pendulum
from airflow.decorators import dag, task

import stg_dds_logic


@dag(
    schedule_interval=None,
    start_date=pendulum.datetime(2024, 5, 30, tz="UTC"),
    catchup=False,
    tags=['pg', 'stg'],
    is_paused_upon_creation=True
)
def stg_to_dds_load():
    @task
    def load_deliverymens_data():
        stg_dds_logic.load_deliveryman_data()

    @task()
    def load_orders_data():
        stg_dds_logic.load_orders_data()

    @task
    def load_delivery_data():
        stg_dds_logic.load_delivery_data()

    deliverymen_task = load_deliverymens_data()
    orders_task = load_orders_data()
    delivery_task = load_delivery_data()

    deliverymen_task >> orders_task >> delivery_task


dds_dag = stg_to_dds_load()
