import pendulum
from airflow.decorators import dag, task

import cdm_logic


@dag(
    schedule_interval=None,
    start_date=pendulum.datetime(2024, 5, 30, tz="UTC"),
    catchup=False,
    tags=['pg', 'stg'],
    is_paused_upon_creation=True
)
def build_cdm():
    @task
    def load_cdm_data():
        cdm_logic.build_data()

    cdm_task = load_cdm_data()

    [cdm_task]


cmd_dag = build_cdm()
