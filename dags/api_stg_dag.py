import pendulum
from airflow.decorators import dag, task

import stg_api_logic


@dag(
    schedule_interval='0/10 * * * *',
    start_date=pendulum.datetime(2024, 5, 30, tz="UTC"),
    catchup=False,
    tags=['pg', 'stg'],
    is_paused_upon_creation=True
)
def load_api_to_stg():
    @task
    def load_mens():
        stg_api_logic.load_mens_data()

    @task
    def load_deliveries():
        stg_api_logic.load_deliveries_data()

    mens_task = load_mens()
    deliveries_task = load_deliveries()

    [mens_task, deliveries_task]


api_stg_dag = load_api_to_stg()
