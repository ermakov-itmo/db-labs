package ru.mermakov.itmo.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import ru.mermakov.itmo.model.Delivery;
import ru.mermakov.itmo.model.DeliveryMan;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class DeliveryService {
    private final ObjectMapper objectMapper;
    private final List<DeliveryMan> mans = new ArrayList<>();
    private final List<Delivery> deliveries = new ArrayList<>();

    @PostConstruct
    private void init() {
        try {
            Resource manResource = new ClassPathResource("deliveryman.json");
            List<DeliveryMan> manList = objectMapper.readValue(manResource.getInputStream().readAllBytes(), new TypeReference<>() {
            });
            mans.addAll(manList);

            Resource deliveryResource = new ClassPathResource("delivery.json");
            List<Delivery> deliveryList = objectMapper.readValue(deliveryResource.getInputStream().readAllBytes(), new TypeReference<>() {
            });
            deliveries.addAll(deliveryList);
        } catch (Exception exception) {
            log.error(exception.getMessage(), exception);
            throw new RuntimeException(exception);
        }
    }

    public List<DeliveryMan> getMans(long limit, long offset) {
        return mans.stream().skip(offset).limit(limit).toList();
    }

    public List<Delivery> getDeliveries(long limit, long offset) {
        return deliveries.stream().skip(offset).limit(limit).toList();
    }
}
