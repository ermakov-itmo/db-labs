package ru.mermakov.itmo.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public record DeliveryMan(
        @JsonProperty("_id")
        String id,
        @JsonProperty("name")
        String name
) implements Serializable {
}
