package ru.mermakov.itmo.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.time.LocalDateTime;

public record Delivery(
        @JsonProperty("order_id")
        String orderId,
        @JsonProperty("order_date_created")
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
        LocalDateTime orderedDate,
        @JsonProperty("delivery_id")
        String deliveryId,
        @JsonProperty("deliveryman_id")
        String deliveryManId,
        @JsonProperty("delivery_address")
        String deliveryAddress,
        @JsonProperty("delivery_time")
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
        LocalDateTime deliveryTime,
        @JsonProperty("rating")
        Short rating,
        @JsonProperty("tips")
        Double tips
) implements Serializable {
}
