package ru.mermakov.itmo.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.mermakov.itmo.model.Delivery;
import ru.mermakov.itmo.model.DeliveryMan;
import ru.mermakov.itmo.service.DeliveryService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class ApiController {
    private final DeliveryService deliveryService;

    @GetMapping("/mans/{limit}/{offset}")
    public List<DeliveryMan> getDeliveryMans(@PathVariable Long limit, @PathVariable Long offset) {
        return deliveryService.getMans(limit, offset);
    }

    @GetMapping("/delivery/{limit}/{offset}")
    public List<Delivery> getDeliveries(@PathVariable Long limit, @PathVariable Long offset) {
        return deliveryService.getDeliveries(limit, offset);
    }
}
