FROM gradle:8-jdk21-alpine as build
WORKDIR /app
COPY --chown=gradle:gradle api /app
RUN gradle --no-daemon bootJar --info

FROM eclipse-temurin:21-jre-alpine
WORKDIR /app
COPY --from=build /app/build/libs/api-1.0-SNAPSHOT.jar app.jar
ENTRYPOINT java -jar app.jar